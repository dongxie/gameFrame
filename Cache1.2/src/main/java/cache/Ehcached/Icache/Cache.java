/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package cache.Ehcached.Icache;


import cache.Ehcached.CacheException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: CLINUX
 * Date: 12-11-25
 * Time: 下午8:26
 * To change this template use File | Settings | File Templates.
 * 缓存接口
 */
public interface Cache {

    /**
     * Get an item from the cache, nontransactionally
     * @param key
     * @return the cached object or <tt>null</tt>
     * @throws cache.Ehcached.CacheException
     */
    public Object get(Object key) throws CacheException;

    /**
     * Add an item to the cache, nontransactionally, with
     * failfast semantics
     * @param key
     * @param value
     * @throws cache.Ehcached.CacheException
     */
    public void put(Object key, Object value) throws CacheException;

    /**
     * Add an item to the cache
     * @param key
     * @param value
     * @throws cache.Ehcached.CacheException
     */
    public void update(Object key, Object value) throws CacheException;

    @SuppressWarnings("rawtypes")
    public List keys() throws CacheException;

    /**
     * Remove an item from the cache
     */
    public void remove(Object key) throws CacheException;

    /**
     * Clear the cache
     */
    public void removeAll() throws CacheException;

    /**
     * Clean up
     */
    public void destroy() throws CacheException;

}

