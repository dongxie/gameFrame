/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.ServerCore.Controller.AbstractController;


import com.dc.gameserver.ServerCore.Service.character.PlayerInstance;
import com.google.protobuf.MessageLite;
import io.netty.buffer.ByteBuf;

import java.io.Serializable;

/**
 * @author :陈磊 <br/>
 * Date: 12-12-14<br/>
 * Time: 下午2:36<br/>
 * connectMethod:13638363871@163.com<br/>
 * buf封装 数据包操作类
 * 数据分发处理接口  以及处理buffer数据
 *
 * 数据包协议类型：1.包长+包类型+包体（消息部分）
 *                  1）包体：length+msg+length+msg ... ...
 *                  注意：操作ChannelBuffer的getxx()方式，method does not modify readerIndex
 *                  readXXX(),writeXX(),将会改变读写指针
 */
public interface IController extends Serializable {

    /****使用数组来存储数据包和处理函数的关系，通过数组下标索引来找到相应的操作处理函数的引用**/
    static final IController[] GAME_CONTROLLERS=new IController[9999];//游戏数据包
    /**服务器初始化的时候加载MessageLite[]***/
    static final MessageLite MESSAGE_LITE[]=new MessageLite[9999];
    /**
     * 数据包分发
     * @param buffer   数据载体
     * @param player     active object
     * 当前游戏服务器中buffer在此函数中释放
     */
    void DispatchByteBuffer(final ByteBuf buffer, final PlayerInstance player) throws Exception;

    /**
     * messageLite数据结构体分发
     * @param messageLite     数据载体
     * @param player active object
     * @throws Exception
     * 注意messageLite应该递交给GC直接处理  ，set null
     */
    void DispatchMessageLit(final MessageLite messageLite, final PlayerInstance player) throws Exception;


}
