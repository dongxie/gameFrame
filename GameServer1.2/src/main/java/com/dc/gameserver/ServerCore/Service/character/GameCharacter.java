/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.ServerCore.Service.character;

import java.io.Serializable;

/**
 * 游戏对象  战斗模型  RPG
 */
public abstract class GameCharacter implements Serializable {}
