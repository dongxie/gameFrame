package com.dc.gameserver.extComponents.Kit.propEncrypt;//package com.dc.gameserver.extComponents.utilsKit.propEncrypt;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// * Created with IntelliJ IDEA.
// * User: noah
// * Date: 9/16/13
// * Time: 3:55 PM
// * To change this template use File | Settings | File Templates.
// */
//public class AesUtils {
//
//    private static Logger logger = LoggerFactory.getLogger(AesUtils.class);
//
//    public static String encrypt(String key, String data) {
//        String result = null;
//        try {
//            result = new AesEncryptor().encryptString2Base64(key, data);
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        }
//        return result;
//    }
//
//    public static String decrypt(String key, String data) {
//        String result = null;
//        try {
//            result = new AesDecryptor().decryptBase642String(key, data);
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        }
//        return result;
//    }
//}
