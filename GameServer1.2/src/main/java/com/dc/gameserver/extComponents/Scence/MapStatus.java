/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Scence;

/**
 * @author :陈磊 <br/>
 * Date: 12-12-9<br/>
 * Time: 下午12:02<br/>
 * connectMethod:13638363871@163.com<br/>
 *
* 枚举对象在地图的状态
        */
public enum MapStatus {
    INTO_MAP, //进入地图
    REDRESS_MAP,//在地图上位置的矫正
    LEAVE_MAP   //离开地图
}
