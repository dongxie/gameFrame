/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package apacheCommonsTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author 石头哥哥 </br>
 *         Project : dcserver1.6</br>
 *         Date: 11/27/13  11</br>
 *         Time: 11:02 PM</br>
 *         Connect: 13638363871@163.com</br>
 *         packageName: apacheCommonsTest</br>
 *         注解：          http://www.oschina.net/question/12_21127
 *         	玩家排列优先级
            大境界高>小境界高>气势上限高>气势下限高>玩家昵称顺序

             多属性权重排序方式！
 */
public class Employee {

    public static void main(String[] args) {
        List<Employee> objs = new ArrayList<Employee>(){{
            add(new Employee(5,3,5000,2));
            add(new Employee(1,9,10000,10));
            add(new Employee(4,5,8000,6));
            add(new Employee(2,9,12000,7));
            add(new Employee(6,1,2000,1));
            add(new Employee(3,5,8000,12));
        }};
        Collections.sort(objs, comparator);
        System.out.println("No\tLevel\tSalary\tYears\n=============================");
        for(Employee a : objs)
            System.out.printf("%d\t%d\t%d\t%d\n",a.id,a.level,a.salary,a.year);
    }

    public Employee(int id, int level, int salary, int year){
        this.id = id;
        this.level = level;
        this.salary = salary;
        this.year = year;
    }

    public int id;
    public int level;
    public int salary;
    public int year;

    private final static Comparator<Employee> comparator = new Comparator<Employee>(){
        @Override
        public int compare(Employee a1, Employee a2) {
            int cr = 0;
            int a = a2.level - a1.level;
            if(a != 0)
                cr = (a>0)?3:-1;
            else{
                a = a2.salary - a1.salary;
                if(a != 0)
                    cr = (a>0)?2:-2;
                else{
                    a = (int)(a2.year - a1.year);
                    if(a != 0)
                        cr = (a>0)?1:-3;
                }
            }
            //System.out.printf("compare(%d,%d)=%d\n", a1.getId(), a2.getId(), cr);
            return cr;
        }
    };
}
