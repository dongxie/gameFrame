/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package net.MinaEngine.core;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LogLevel;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author :石头哥哥<br/>
 *         Project:FrameServer1.0
 *         Date: 13-5-19
 *         Time: 下午5:24
 */
public class MServer {
    private static final Logger LOG = LoggerFactory.getLogger(MServer.class);  //logger
    NioSocketAcceptor netAcceptor=null;

    public MServer(){ }

    /**
     * Sets the handler which will handle all connections managed by this service.
     * 启动游戏服务器
     */
    public void startService() throws Exception {

       boolean status=false;
        if (status){
            /**
             *  Constructor for {@link org.apache.mina.transport.socket.nio.NioSocketAcceptor} using default parameters value  ==1, and
             * given number of {@link org.apache.mina.transport.socket.nio.NioProcessor} for multithreading I/O operations.
             *  NioSocketAcceptor==1
             * NioProcessor value== cpu+1
             * NioSocketAcceptor  构造函数参数是        处理 NioProcessor  I/O线程的数量
             */
            netAcceptor=new NioSocketAcceptor(Runtime.getRuntime().availableProcessors()+1);

            IoBuffer.setUseDirectBuffer(false);
           /***filter chain config*/
           DefaultIoFilterChainBuilder chainBuilder=netAcceptor.getFilterChain();

           /**mina事件处理日志filter*/
           LoggingFilter loggingFilter = new LoggingFilter();
           loggingFilter.setMessageReceivedLogLevel(LogLevel.NONE);
           loggingFilter.setMessageSentLogLevel(LogLevel.NONE);
           loggingFilter.setSessionClosedLogLevel(LogLevel.NONE);
           loggingFilter.setSessionCreatedLogLevel(LogLevel.NONE);
           loggingFilter.setSessionOpenedLogLevel(LogLevel.NONE);
           loggingFilter.setSessionIdleLogLevel(LogLevel.NONE);
           loggingFilter.setExceptionCaughtLogLevel(LogLevel.NONE);

           chainBuilder.addLast("logger",loggingFilter);

            /**数据的编解码,IoProcessor 之后的第一个过滤器*/
            chainBuilder.addLast("protocolCodecFilter",new ProtocolCodecFilter(new MServerProtocolCodecFactory()));

          /* ExecutorFilter 的典型场景是将业务逻辑（譬如：耗时的数
           据库操作）放在单独的线程中运行，也就是说与IO 处理无关的操作可以考虑使用
           ExecutorFilter 来异步执行。*/
//           chainBuilder.addLast("executors",new ExecutorFilter(MServerConfig.corePoolSize,
//                   MServerConfig.corePoolSize,MServerConfig.DEFAULT_KEEPALIVE_TIME, TimeUnit.SECONDS,
//                   new PriorityThreadFactory("executionHandlerThread",Thread.NORM_PRIORITY)));//业务处理线程池模型
//
//            /***Handler，通过adapter接入了session的整个生命周期*/
//           /**Sets the handler which will handle all connections managed by this service.*/
//           netAcceptor.setHandler(new MServerIoHandler());
//
//            /***session config*/
//           SocketSessionConfig sessionConfig=netAcceptor.getSessionConfig();
//           sessionConfig.setReuseAddress(true);   //地址重用
//           sessionConfig.setKeepAlive(true);
//           sessionConfig.setTcpNoDelay(true);
//           sessionConfig.setIdleTime(IdleStatus.READER_IDLE,
//                   MServerConfig.SESSION_IDLE); //readTimeout
//
//           //sessionConfig 流量统计
//           sessionConfig.setThroughputCalculationInterval(5);//default value of 3  seconds
//
//
//           netAcceptor.bind(new InetSocketAddress(MServerConfig.GameServerPort));

       }else {
           LOG.warn("startService failed!");
       }
    }
}
