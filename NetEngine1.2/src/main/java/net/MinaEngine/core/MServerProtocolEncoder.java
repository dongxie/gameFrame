/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package net.MinaEngine.core;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

/**
 * @author :石头哥哥<br/>
 *         Project:FrameServer1.0
 *         Date: 13-5-19
 *         Time: 下午4:26
 */
public class MServerProtocolEncoder extends ProtocolEncoderAdapter {
    /**
     * Encodes higher-level message objects into binary or protocol-specific data.
     * MINA invokes {@link #encode(org.apache.mina.core.session.IoSession, Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}
     * method with message which is popped from the session write queue, and then
     * the encoder implementation puts encoded messages (typically {@link org.apache.mina.core.buffer.IoBuffer}s)
     * into {@link org.apache.mina.filter.codec.ProtocolEncoderOutput}.
     *
     * @throws Exception if the message violated protocol specification
     */
    @Override
    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
        IoBuffer buf = IoBuffer.allocate(0x40).setAutoExpand(true);
        buf.putObject(message);
        buf.flip();
        out.write(buf); //add messageQueue ｛ProtocolEncoderOutput｝
    }
}
