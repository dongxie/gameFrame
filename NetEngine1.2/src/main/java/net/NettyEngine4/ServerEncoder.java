/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package net.NettyEngine4;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;

/**
 * Created with IntelliJ IDEA.
 * User: 石头哥哥  </br>
 * Date: 13-10-22            </br>
 * Time: 下午4:55              </br>
 * Package_name: Server.ExtComponents.NettyEngine4   </br>
 * 注意encoder编码byteBuffer类型                     </br>
 * 编码在构建数据包完成 （逻辑层处理完毕）     </br>
 */
public class ServerEncoder extends ChannelOutboundHandlerAdapter {
    /**
     * 在业务数据逻辑层 ，将要发送的数据编码完成！
     * 因为写入数据到buffer中，所以永远方法isReadable（）会返回true </br>
     * 而netty底层将会在数据发送完毕之后将数据msg释放掉，设置为NULL,让  </br>
     * GC直接来回收内存                   </br>
     * 数据协议的编码在逻辑层已经封装,msg 默认类型为:PooledHeapByteBuf </br>
     * @param ctx               </br>
     * @param msg      </br>
     * @param promise       </br>
     * @throws Exception          </br>
     */
    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception{
        ctx.write(msg,promise);
    }
}
